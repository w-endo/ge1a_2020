#include "Bullet.h"
#include "Engine/Model.h"
#include "Engine/SphereCollider.h"


//コンストラクタ
Bullet::Bullet(GameObject * parent)
	:GameObject(parent, "Bullet"), hModel_(-1),	
	move_(XMVectorSet(0,0,1,0)),life_(120),
	GRAVITY(0.05f), SPEED(2.0f)
{
}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Bullet.fbx");
	assert(hModel_ >= 0);

	SphereCollider* collision = new SphereCollider(XMVectorSet(0, 0, 0, 0),0.5f);
	AddCollider(collision);

}

//更新
void Bullet::Update()
{
	transform_.position_ += move_;
	move_.vecY -= GRAVITY;

	life_--;
	if (life_ == 0)
	{
		KillMe();
	}
}

//描画
void Bullet::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Bullet::Release()
{
}

void Bullet::SetMove(XMVECTOR move)
{
	move_ = XMVector3Normalize(move) * SPEED;
}
