#include "TankHead.h"
#include "Engine/Model.h"
#include "Bullet.h"
#include "Engine/input.h"

//コンストラクタ
TankHead::TankHead(GameObject * parent)
	:GameObject(parent, "TankHead"), hModel_(-1)
{
}

//デストラクタ
TankHead::~TankHead()
{
}

//初期化
void TankHead::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("TankHead.fbx");
	assert(hModel_ >= 0);
}

//更新
void TankHead::Update()
{
	if (Input::IsKeyDown(DIK_SPACE))
	{
		Bullet* bullet = Instantiate<Bullet>(FindObject("PlayScene"));
		XMVECTOR cannonPos = Model::GetBonePosition(hModel_, "CannonPos");
		XMVECTOR cannonRoot = Model::GetBonePosition(hModel_, "CannonRoot");
		bullet->SetPosition(cannonPos);


		bullet->SetMove(cannonPos - cannonRoot);
	}
}

//描画
void TankHead::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void TankHead::Release()
{
}