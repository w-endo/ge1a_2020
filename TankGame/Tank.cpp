#include "Tank.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Engine/Camera.h"
#include "Ground.h"
#include "TankHead.h"


//#define ROTATE_SPEED	3

//コンストラクタ
Tank::Tank(GameObject * parent)
	:GameObject(parent, "Tank"), hModel_(-1), ROTATE_SPEED(3)
{
}

//デストラクタ
Tank::~Tank()
{
}

//初期化
void Tank::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("TankBody.fbx");
	assert(hModel_ >= 0);

	Instantiate<TankHead>(this);
}

//更新
void Tank::Update()
{
	//戦車の移動
	Move();

	//カメラの位置
	CameraMove();

	//高さを地面に合わせる
	AlignHeight();
}

void Tank::AlignHeight()
{

	Ground* pGround = (Ground*)FindObject("Ground");    //ステージオブジェクトを探す
	int hGroundModel = pGround->GetModelHandle();    //モデル番号を取得

													 //レイキャスト
	RayCastData data;
	data.start = transform_.position_;   //レイの発射位置
	data.start.vecY = 0;				 //発射位置を標高0ｍにする
	data.dir = XMVectorSet(0, -1, 0, 0); //レイの方向
	Model::RayCast(hGroundModel, &data); //レイを発射

										 //レイが当たったら
	if (data.hit)
	{
		//戦車の高さを正しい標高にする
		transform_.position_.vecY = -data.dist;
	}
}

void Tank::CameraMove()
{
	XMMATRIX m;
	m = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));

	////一人称視点
	//Camera::SetPosition(transform_.position_);
	//Camera::SetTarget(transform_.position_ + move);

	//三人称視点
	XMVECTOR cam = { 0, 5, -10, 0 };
	cam = XMVector3TransformCoord(cam, m);	//回転あり
	Camera::SetPosition(transform_.position_ + cam);
	Camera::SetTarget(transform_.position_ + XMVectorSet(0, 3, 0, 0));


	////固定カメラ
	//Camera::SetPosition(XMVectorSet(0, 110, -40, 0));
	//Camera::SetTarget(XMVectorSet(0, 0, -8, 0));


	////定点カメラ
	//Camera::SetPosition(XMVectorSet(0, 30, -30, 0));
	//Camera::SetTarget(transform_.position_);
}



void Tank::Move()
{
	if (Input::IsKey(DIK_A))
	{
		transform_.rotate_.vecY -= ROTATE_SPEED;
	}

	if (Input::IsKey(DIK_D))
	{
		transform_.rotate_.vecY += ROTATE_SPEED;
	}

	XMVECTOR move = { 0, 0, 0.3f, 0 };

	XMMATRIX m;
	m = XMMatrixRotationY(XMConvertToRadians(transform_.rotate_.vecY));

	move = XMVector3TransformCoord(move, m);

	if (Input::IsKey(DIK_W))
	{
		transform_.position_ += move;
	}
	if (Input::IsKey(DIK_S))
	{
		transform_.position_ -= move;
	}

}

//描画
void Tank::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Tank::Release()
{
}