#include "Player.h"
#include "Engine/Model.h"
#include "Engine/Input.h"
#include "Bullet.h"

//コンストラクタ
Player::Player(GameObject * parent)
	:GameObject(parent, "Player"), hModel_(-1)
{

}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Player.fbx");
	assert(hModel_ >= 0);
}

//更新
void Player::Update()
{
	if (Input::IsKeyDown(DIK_SPACE))
	{
		Bullet* bullet = Instantiate<Bullet>(GetParent());
		bullet->SetPosition(transform_.position_);
	}
	   	 
	//→キーが押されていたら
	if (Input::IsKey(DIK_RIGHT))
	{
		transform_.position_.vecX += 0.1f;
		transform_.rotate_.vecZ = -20.0f;
	}

	//←キーが押されていたら
	else if (Input::IsKey(DIK_LEFT))
	{
		transform_.position_.vecX -= 0.1f;
		transform_.rotate_.vecZ = 20.0f;
	}

	else
	{
		transform_.rotate_.vecZ *= 0.9f;
	}



}

//描画
void Player::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{
}
