#include "Player.h"
#include "Engine/Model.h"
#include "Engine/Input.h"

//コンストラクタ
Player::Player(GameObject * parent)
	:GameObject(parent, "Player"),
	hModel_(-1),pStage(nullptr)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("Player.fbx");
	assert(hModel_ >= 0);

	transform_.position_.vecX = 6.5f;
	transform_.position_.vecZ = 6.5f;

	pStage = (Stage*)FindObject("Stage");
	assert(pStage != nullptr);
}

//更新
void Player::Update()
{
	XMVECTOR move = { 0, 0, 0, 0 };

	move.vecX = Input::GetPadStickL().vecX;
	move.vecZ = Input::GetPadStickL().vecY;

	if (Input::IsKey(DIK_RIGHT) || 
		Input::IsPadButton(XINPUT_GAMEPAD_DPAD_RIGHT))
	{
		//transform_.position_.vecX += 0.1f;
		move.vecX = 1;
	}
	if (Input::IsKey(DIK_LEFT) ||
		Input::IsPadButton(XINPUT_GAMEPAD_DPAD_LEFT))
	{
		//transform_.position_.vecX -= 0.1f;
		move.vecX = -1;
	}
	if (Input::IsKey(DIK_UP) ||
		Input::IsPadButton(XINPUT_GAMEPAD_DPAD_UP))
	{
		//transform_.position_.vecZ += 0.1f;
		move.vecZ = 1;
	}
	if (Input::IsKey(DIK_DOWN) ||
		Input::IsPadButton(XINPUT_GAMEPAD_DPAD_DOWN))
	{
		//transform_.position_.vecZ -= 0.1f;
		move.vecZ = -1;
	}

	if (XMVector3Length(move).vecX != 0)
	{

		XMVECTOR front = { 0,0,1,0 };

		move = XMVector3Normalize(move);

		float dot = XMVector3Dot(front, move).vecX;
		float angle = acos(dot);

		XMVECTOR cross = XMVector3Cross(front, move);
		if (cross.vecY < 0)
			angle *= -1;

		transform_.rotate_.vecY = XMConvertToDegrees(angle);
	}

	transform_.position_ += move * 0.3f;


	int cx, cz;

	//右
	{
		cx = (int)(transform_.position_.vecX + 0.3f);
		cz = (int)transform_.position_.vecZ;
		if (pStage->GetMapData(cx, cz) == TYPE_WALL)
		{
			transform_.position_.vecX = (float)cx - 0.3f;
		}
	}

	//左
	{
		cx = (int)(transform_.position_.vecX - 0.3f);
		cz = (int)transform_.position_.vecZ;
		if (pStage->GetMapData(cx, cz) == TYPE_WALL)
		{
			transform_.position_.vecX = (float)cx + 1.3f;
		}
	}

	//奥
	{
		cx = (int)transform_.position_.vecX;
		cz = (int)(transform_.position_.vecZ + 0.3f);
		if (pStage->GetMapData(cx, cz) == TYPE_WALL)
		{
			transform_.position_.vecZ -= move.vecZ * 0.3f;
		}
	}

	//前
	{
		cx = (int)transform_.position_.vecX;
		cz = (int)(transform_.position_.vecZ - 0.3f);
		if (pStage->GetMapData(cx, cz) == TYPE_WALL)
		{
			transform_.position_.vecZ -= move.vecZ * 0.3f;
		}
	}
}

//描画
void Player::Draw()
{
	Model::SetTransform(hModel_, transform_);
	Model::Draw(hModel_);
}

//開放
void Player::Release()
{
}