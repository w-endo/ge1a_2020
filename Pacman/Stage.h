#pragma once
#include "Engine/GameObject.h"
#include "Engine/Model.h"

enum
{
	TYPE_FLOOR,
	TYPE_WALL,
	TYPE_MAX
};


//ステージを管理するクラス
class Stage : public GameObject
{
	int hModel_[TYPE_MAX];    //モデル番号

	int mapData[15][15];

public:
	//コンストラクタ
	Stage(GameObject* parent);

	//デストラクタ
	~Stage();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;

	int GetMapData(int x, int z);
};