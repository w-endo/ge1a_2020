#include "Stage.h"
#include "Engine/Camera.h"
#include "Engine/CsvReader.h"

//コンストラクタ
Stage::Stage(GameObject * parent)
	:GameObject(parent, "Stage")
{
}

//デストラクタ
Stage::~Stage()
{
}

//初期化
void Stage::Initialize()
{
	//モデルデータのロード
	hModel_[TYPE_FLOOR] = Model::Load("Floor.fbx");
	assert(hModel_[TYPE_FLOOR] >= 0);

	hModel_[TYPE_WALL] = Model::Load("Wall.fbx");
	assert(hModel_[TYPE_WALL] >= 0);

	Camera::SetPosition(XMVectorSet(7.5, 10, -7.5, 0));
	Camera::SetTarget(XMVectorSet(7.5, 0, 7.5, 0));

//	ZeroMemory(mapData, sizeof(mapData));
	CsvReader csv;
	csv.Load("mapData.csv");
	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			mapData[x][z] = csv.GetValue(x, z);
		}
	}


}

//更新
void Stage::Update()
{
}

//描画
void Stage::Draw()
{
	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			Transform trans;
			trans.position_.vecX = x;
			trans.position_.vecZ = z;

			//床
			Model::SetTransform(hModel_[TYPE_FLOOR], trans);
			Model::Draw(hModel_[TYPE_FLOOR]);

			//壁
			if (mapData[x][z] == TYPE_WALL)
			{
				Model::SetTransform(hModel_[TYPE_WALL], trans);
				Model::Draw(hModel_[TYPE_WALL]);
			}
		}
	}
}

//開放
void Stage::Release()
{
}

int Stage::GetMapData(int x, int z)
{
	return mapData[x][z];
}
